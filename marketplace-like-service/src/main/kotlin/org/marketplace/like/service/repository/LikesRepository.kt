package org.marketplace.like.service.repository

import org.jetbrains.exposed.sql.transactions.transaction
import org.marketplace.like.model.Like
import org.marketplace.like.model.LikeByProductRequest
import org.marketplace.like.model.response.LikeResponse
import org.marketplace.like.model.response.ProductLikesResponse

object LikesRepository {

  fun likeUnlike(like: Like): LikeResponse {
    val result = LikeResponse(0)
    transaction {
      val query = "select like_unlike(${like.productId}, ${like.userId})"
      exec(query) { rs ->
        while (rs.next()) {
          result.count = rs.getInt(1)
        }
      }
    }
    return result
  }

  fun likeById(product: LikeByProductRequest): ProductLikesResponse {
    val result = ProductLikesResponse(productId = product.productId, likes = 0)
    transaction {
      val query = "select likes_by_product(${product.productId})"
      exec(query) { rs ->
        while (rs.next()) {
          result.likes = rs.getInt(1)
        }
      }
    }
    return result
  }
}




