package org.marketplace.like.service.route

import io.ktor.server.application.*
import io.ktor.server.routing.*
import org.marketplace.like.service.controller.LikeController

fun Application.likeById() {
    routing {
      post("/product_like") {
       val likeController = LikeController(call)
        likeController.likeById()
    }
  }
}
