package org.marketplace.like.service


import com.typesafe.config.ConfigFactory
import io.ktor.server.cio.*
import io.ktor.server.engine.*
import org.marketplace.like.service.config.DBConfig
import org.marketplace.like.service.plugins.configureSerialization
import org.marketplace.like.service.route.likeById
import org.marketplace.like.service.route.likeUnlike
import org.marketplace.like.service.route.testHealth


fun main() {
  val conf = ConfigFactory.load()

  DBConfig.init()
  embeddedServer(CIO, port = conf.getInt("DataBasePort")) {
    configureSerialization()
    testHealth()
    likeUnlike()
    likeById()
  }.start(wait = true)

}
