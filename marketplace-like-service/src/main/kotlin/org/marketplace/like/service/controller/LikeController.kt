package org.marketplace.like.service.controller

import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import org.marketplace.like.model.Like
import org.marketplace.like.model.LikeByProductRequest
import org.marketplace.like.service.repository.LikesRepository

class LikeController(private val call: ApplicationCall) {


  suspend fun likeUnlike() {
    val request = call.receive<Like>()
    val res = LikesRepository.likeUnlike(
      Like(
        userId = request.userId,
        productId = request.productId
      )
    )
    call.respond(res)
  }

  suspend fun likeById() {
    val request = call.receive<LikeByProductRequest>()
    val res = LikesRepository.likeById(LikeByProductRequest(productId = request.productId))
    call.respond(res)
  }
}
