package org.marketplace.like.service.config

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.slf4j.LoggerFactory

object DBConfig {
  private val conf = ConfigFactory.load()
  val logger = LoggerFactory.getLogger(javaClass)
  fun init() {
    Database.connect(hikari())
    logger.info(conf.getString("DatabaseUrl"))
    logger.info(conf.getString("DatabaseUser"))
    logger.info(conf.getString("DatabasePassword"))
    logger.info(conf.getString("DatabaseDriver"))
    logger.info(conf.getInt("DataBasePort").toString())
    logger.info(conf.getString("DataBaseHost"))
  }


  private fun hikari(): HikariDataSource {
    val config = HikariConfig()
    config.jdbcUrl = conf.getString("DatabaseUrl")
    config.username = conf.getString("DatabaseUser")
    config.password = conf.getString("DatabasePassword")
    config.driverClassName = conf.getString("DatabaseDriver")
    config.maximumPoolSize = 10
    config.isAutoCommit = false
    return HikariDataSource(config)
  }

}
