package org.marketplace.like.service.route

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*


fun Application.testHealth() {

  routing {
    get("/test") {
      call.respondText { "i`m fine" }
    }
  }
}
