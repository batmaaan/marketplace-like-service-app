CREATE OR REPLACE FUNCTION likes_by_product(
  productId NUMERIC
)
  RETURNS NUMERIC AS
$$
DECLARE
  row_exist NUMERIC;
BEGIN
  SELECT 1
  INTO row_exist
  FROM likes
  WHERE product_id = productId;
  RETURN (SELECT COUNT(*) FROM likes WHERE product_id = productId);
END;

$$
  LANGUAGE plpgsql;;

