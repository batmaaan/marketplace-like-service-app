create table likes
(
  product_id integer not null,
  user_id    integer not null
) partition by hash (product_id);


CREATE TABLE likes_0 PARTITION OF likes FOR VALUES WITH (MODULUS 4, REMAINDER 0);
CREATE TABLE likes_1 PARTITION OF likes FOR VALUES WITH (MODULUS 4, REMAINDER 1);
CREATE TABLE likes_2 PARTITION OF likes FOR VALUES WITH (MODULUS 4, REMAINDER 2);
CREATE TABLE likes_3 PARTITION OF likes FOR VALUES WITH (MODULUS 4, REMAINDER 3);

