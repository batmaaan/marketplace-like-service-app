CREATE OR REPLACE FUNCTION like_unlike(
  productId NUMERIC,
  userId NUMERIC
)
  RETURNS NUMERIC AS
$$
DECLARE
  row_exist NUMERIC;
BEGIN
      SELECT 1
    INTO row_exist
    FROM likes
    WHERE product_id = productId
      AND user_id = userId;

    if (row_exist > 0) THEN
      DELETE FROM likes WHERE product_id = productId AND user_id = userId;
      RETURN (SELECT COUNT(user_id) FROM likes WHERE product_id = productId);
    ELSE
      INSERT INTO likes(product_id, user_id) VALUES (productId, userId);
      RETURN (SELECT COUNT(user_id) FROM likes WHERE product_id = productId);
    END IF;
END;

$$

  LANGUAGE plpgsql;;

