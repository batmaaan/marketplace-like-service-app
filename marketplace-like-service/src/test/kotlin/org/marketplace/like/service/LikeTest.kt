package org.marketplace.like.service

import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.marketplace.like.client.LikeServiceClientImpl
import org.marketplace.like.model.Like
import org.marketplace.like.model.LikeByProductRequest
import org.marketplace.like.model.response.LikeResponse
import org.marketplace.like.model.response.ProductLikesResponse
import org.marketplace.like.service.repository.LikesRepository
import org.marketplace.utils.HOST
import org.marketplace.utils.PORT
import javax.sql.DataSource
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


internal class LikeTest {
  private lateinit var dataSource: DataSource


  @BeforeEach
  fun setUp() {
    dataSource = TestDB.setup()
    Database.connect(dataSource)
  }

  @Test
  fun likeUnlike() {
    val resultOne = LikesRepository.likeUnlike(Like(1, 1))
    assertEquals(LikeResponse(1), resultOne)
    val resultZero = LikesRepository.likeUnlike(Like(1, 1))
    assertEquals(LikeResponse(0), resultZero)
  }

  @Test
  fun likeUnlikeClient() {
    val URL = "http://$HOST:$PORT"
    val result = LikeServiceClientImpl(URL).likeUnlike(Like(1,1))
    assertNotNull(result)
  }

  @Test
  fun likeByIdTest(){
    val like = LikesRepository.likeUnlike(Like(1, 1))
    val result = LikesRepository.likeById(LikeByProductRequest(1))
    assertEquals(ProductLikesResponse(1, 1), result);
  }
}

