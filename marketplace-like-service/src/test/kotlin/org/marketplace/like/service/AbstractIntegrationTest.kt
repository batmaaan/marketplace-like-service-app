package org.marketplace.like.service

import org.marketplace.utils.HOST
import org.marketplace.utils.PORT
import org.postgresql.ds.PGSimpleDataSource
import org.testcontainers.containers.PostgreSQLContainer
import javax.sql.DataSource

object TestDB {

  private val POSTGRES = PostgreSQLContainer("postgres:12")
    .withInitScript("db/init_test_db.sql")

  fun setup(): DataSource {
    POSTGRES.start()
    HOST= POSTGRES.host
    PORT = POSTGRES.firstMappedPort.toString()
    return setupDataSource(POSTGRES)
  }

  private fun setupDataSource(postgreSQLContainer: PostgreSQLContainer<*>): DataSource {
    return PGSimpleDataSource().apply {
      setURL(postgreSQLContainer.jdbcUrl)
      user = postgreSQLContainer.username
      password = postgreSQLContainer.password
    }
  }

}
