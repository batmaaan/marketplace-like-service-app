create table likes
(
  product_id integer not null,
  user_id    integer not null
) partition by hash (product_id);


CREATE TABLE likes_0 PARTITION OF likes FOR VALUES WITH (MODULUS 4, REMAINDER 0);
CREATE TABLE likes_1 PARTITION OF likes FOR VALUES WITH (MODULUS 4, REMAINDER 1);
CREATE TABLE likes_2 PARTITION OF likes FOR VALUES WITH (MODULUS 4, REMAINDER 2);
CREATE TABLE likes_3 PARTITION OF likes FOR VALUES WITH (MODULUS 4, REMAINDER 3);

CREATE OR REPLACE FUNCTION like_unlike(
  productId NUMERIC,
  userId NUMERIC
)
  RETURNS NUMERIC AS
'
  DECLARE
    row_exist NUMERIC;
  BEGIN
    SELECT 1
    INTO row_exist
    FROM likes
    WHERE product_id = productId
      AND user_id = userId;

    if (row_exist > 0) THEN
      DELETE
      FROM likes
      WHERE product_id = productId
        AND user_id = userId;
      RETURN (SELECT COUNT(user_id)
              FROM likes
              WHERE product_id = productId);
    ELSE
      INSERT INTO likes(product_id, user_id)
      VALUES (productId, userId);
      RETURN (SELECT COUNT(user_id)
              FROM likes
              WHERE product_id = productId);
    END IF;
  END;

'
  LANGUAGE plpgsql;;
CREATE OR REPLACE FUNCTION likes_by_product(
  productId NUMERIC
)
  RETURNS NUMERIC AS
'
DECLARE
  row_exist NUMERIC;
BEGIN
  SELECT 1
  INTO row_exist
  FROM likes
  WHERE product_id = productId;
  RETURN (SELECT COUNT(*) FROM likes WHERE product_id = productId);
END;

'
  LANGUAGE plpgsql;;
