package org.marketplace.like.model.response

import kotlinx.serialization.Serializable

@Serializable
data class ProductLikesResponse(
  var productId: Int,
  var likes: Int
)
