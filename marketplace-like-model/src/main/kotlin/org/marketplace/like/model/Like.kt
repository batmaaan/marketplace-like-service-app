package org.marketplace.like.model

import kotlinx.serialization.Serializable


@Serializable
data class Like(
  val userId: Int,
  val productId: Int
)

