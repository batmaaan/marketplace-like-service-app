package org.marketplace.like.model

import kotlinx.serialization.Serializable

@Serializable
data class LikeByProductRequest(val productId:Int)
