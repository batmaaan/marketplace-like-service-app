package org.marketplace.like.model.response

import kotlinx.serialization.Serializable

@Serializable
data class LikeResponse(
  var count: Int
)
