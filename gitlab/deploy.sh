mvn clean compile deploy
mvn compile -pl marketplace-like-service -U com.google.cloud.tools:jib-maven-plugin:3.2.1:build -Djib.to.auth.username=$DOCKER_USERNAME -Djib.to.auth.password=$DOCKER_PASS  -Dimage=batmaaan/marketplace-like-service:1.0-SNAPSHOT
cd /home/finch/app/like; docker-compose pull; docker-compose stop; docker-compose up -d;
