package org.marketplace.like.client

import com.beust.klaxon.Klaxon
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.future.future

import org.marketplace.like.model.Like
import org.marketplace.like.model.LikeByProductRequest
import org.marketplace.like.model.response.LikeResponse
import org.marketplace.like.model.response.ProductLikesResponse
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper
import java.util.concurrent.CompletableFuture

class LikeServiceClientImpl(private val url: String) : LikeServiceClient {

  private val client = HttpClient(OkHttp)

  @OptIn(DelicateCoroutinesApi::class)
  override fun likeUnlike(like: Like): CompletableFuture<LikeResponse?> {
    val objectMapper = ObjectMapper()
    val requestBody: String = objectMapper
      .writeValueAsString(like)
    val URI = "$url/like"

    return GlobalScope.future {
      val response = client.post {
        url(URI)
        contentType(ContentType.Application.Json)
        header(HttpHeaders.ContentType, ContentType.Application.Json)
        setBody(requestBody)
      }
      val responseBody: String = response.body()

      Klaxon().parse<LikeResponse>(responseBody)
    }
  }

  @OptIn(DelicateCoroutinesApi::class)
  override fun likeById(product: LikeByProductRequest): CompletableFuture<ProductLikesResponse?> {
    val objectMapper = ObjectMapper()
    val requestBody: String = objectMapper
      .writeValueAsString(product)
    val URI = "$url/product_like"

    return GlobalScope.future {
      val response = client.post {
        url(URI)
        contentType(ContentType.Application.Json)
        header(HttpHeaders.ContentType, ContentType.Application.Json)
        setBody(requestBody)
      }
      val responseBody: String = response.body()

      Klaxon().parse<ProductLikesResponse>(responseBody)
    }
  }


}

