package org.marketplace.like.client

import kotlinx.coroutines.DelicateCoroutinesApi
import org.marketplace.like.model.Like
import org.marketplace.like.model.LikeByProductRequest
import org.marketplace.like.model.response.LikeResponse
import org.marketplace.like.model.response.ProductLikesResponse
import java.util.concurrent.CompletableFuture

interface LikeServiceClient {


  @OptIn(DelicateCoroutinesApi::class)
  fun likeUnlike(like: Like): CompletableFuture<LikeResponse?>
  fun likeById(product: LikeByProductRequest): CompletableFuture<ProductLikesResponse?>
}
